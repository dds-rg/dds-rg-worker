#!/usr/bin/env bash

docker build -t registry.gitlab.com/dds-rg/dds-rg-worker .
docker push registry.gitlab.com/dds-rg/dds-rg-worker