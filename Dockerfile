FROM node:dubnium-stretch

ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir /usr/src/app -p
WORKDIR /usr/src/app

COPY ./src /usr/src/app

RUN cd /usr/src/app; rm -rf node_modules; npm i

EXPOSE 3000

CMD ["node", "index.js"]