#!/usr/bin/env bash

rm -rf src/node_modules
docker build . --tag dds-rg-worker
docker run -it --rm --privileged -p 3000:3000 --env-file ./env --name dds-rg-worker dds-rg-worker
