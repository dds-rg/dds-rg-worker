'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()
const basicAuth = require('~/lib/auth/basic')
const placeDetails = require('~/lib/placeDetails')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.post(
    '/api/placeDetails',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            miningLocationId: {
              type: 'integer'
            }
          },
          required: [
            'miningLocationId'
          ]
        }
      }
    },
    async function (req, reply) {

      let places
      let miningLocationId

      try {
        miningLocationId = req.body.miningLocationId

        places = (await db.sequelize.query(
          `
        select * from vw_master_list_entity_lookups a where a."miningLocationId" = :miningLocationId
        `,
          {
            replacements: {
              miningLocationId
            }
          }
        ))[0]

        await placeDetails.lookupDetails(places)
      } catch (err) {
        log.warn(`${err.message}`)
      }

      reply.send({
        placesLength: places.length
      })

    })

  next()

}