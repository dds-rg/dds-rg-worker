'use strict'

const db = require('~/lib/db')
const basicAuth = require('~/lib/auth/basic')
const log = require('~/lib/logger')()
const Place = require('~/lib/models/place')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.post(
    '/api/place/lookupByMiningLocationId',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            id: {
              type: 'integer'
            }
          },
          required: [
            'id'
          ]
        }
      }
    },
    async function (req, reply) {

      try {
        const miningLocationId = req.body.id
        const miningLocation = await db.models.MiningLocation.findOne({
          where: {
            id: miningLocationId
          }
        })

        if (!miningLocation) {
          return reply.code(400).send(new Error(`Mining Location ${miningLocationId} not found.`))
        }

        await Place.lookupByMiningLocation(miningLocation)

        log.debug(`finished place lookup for "${miningLocation.name}"`)

        reply.send(miningLocation)
      } catch (err) {
        log.error(err.message)
        log.debug(err.stack)
        reply.send(err)
      }


    })

  fastify.post(
    '/api/place/lookupAllPlaces',
    async function (req, reply) {

      await Place.lookupAllPlaces()

      log.debug(`finished place lookup for all mining locations`)

      reply.send()

    })

  next()

}