'use strict'

const basicAuth = require('~/lib/auth/basic')
const db = require('~/lib/db')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.get('/', async function(req, reply) {

    reply.view('/pages/slash/index', {

    })

  })

  next()

}