'use strict'

const basicAuth = require('~/lib/auth/basic')
const db = require('~/lib/db')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.get('/internalPlaces', async function(req, reply) {

    const internalPlaces = (await db.sequelize.query(
      `
      SELECT *
        FROM
          VW_INTERNAL_LOCATION_INFO A
        ORDER BY A.NAME ASC`
    ))[0]

    reply.view('/pages/internalPlaces/index', {
      internalPlaces
    })
  })

  next()

}