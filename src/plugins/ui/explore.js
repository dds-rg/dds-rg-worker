'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()
const basicAuth = require('~/lib/auth/basic')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.get('/explore', async function (req, reply) {

    const tables = [
      {
        name: 'Mining Locations',
        tableName: 'MiningLocation'
      },
      {
        name: 'Social Providers',
        tableName: 'SocialProvider'
      },
      {
        name: 'Master Lists',
        tableName: 'MasterList'
      },
      {
        name: 'Master List Entities',
        tableName: 'MasterListEntity'
      },
      {
        name: 'Place (general record for a place)',
        tableName: 'Place'
      }
    ]

    reply.view('/pages/explore/index', {
      tables
    })

  })

  fastify.get('/explore/:table', async function (req, reply) {

    let data
    let realRows

    try {
      data = await db.models[req.params.table].findAll()
      realRows = data.map(function (v) {
        return v.dataValues
      })
    } catch (err) {
      log.error(err.message)
      log.debug(err.stack)
    } finally {
      reply.view('/pages/exploreTable/index', {
        rows: realRows,
        columns: Object.keys(data[0].dataValues),
        table: req.params.table
      })
    }

  })

  next()

}