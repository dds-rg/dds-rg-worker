'use strict'

const basicAuth = require('~/lib/auth/basic')
const db = require('~/lib/db')

module.exports = function (fastify, opts, next) {

  basicAuth.addHook(fastify)

  fastify.get('/miningLocations', async function(req, reply) {

    const miningLocations = await db.models.MiningLocation.findAll()

    reply.view('/pages/miningLocations/index', {
      miningLocations
    })
  })

  next()

}