$(document).ready(function () {

  function appendAlert(_message, _type) {
    if (!_type) {
      _type = 'warning';
    }
    // $('#divAlerts').append("<p>1</p>");
    $('#divAlerts').append('<div class="alert alert-' + _type + ' alert-dismissible fade show" role="alert"> ' + _message + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>');


  }

  function getMiningLocationId() {
    var selectMiningLocation = $('#selectMiningLocation')[0];
    var miningLocationId = parseInt(selectMiningLocation.value);
    console.log('miningLocationId: %s', miningLocationId);
    if (!miningLocationId || miningLocationId === 0) {
      appendAlert('You need to actually select a Mining Location.');
      $('#selectMiningLocation').focus();
      return false;
    } else {
      return miningLocationId;
    }
  }

  $('#btnPlaceDetailScrape').click(function () {
    var miningLocationId = getMiningLocationId()
    if (miningLocationId) {
      $('#spinnerBtnPlaceDetailScrape').css('display', 'inline-block');
      $.ajax({
        url: '/api/placeDetails',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
          miningLocationId: miningLocationId
        }),
        complete: function (xhr, status) {
          if (xhr.status === 200) {
            appendAlert(xhr.responseJSON.placesLength + '&nbsp; <code>Master List Entities</code> &nbsp; were scraped!', 'success');
          } else {
            appendAlert('Sorry, something didn\'t work right.', 'danger');
          }
          $('#spinnerBtnPlaceDetailScrape').css('display', 'none');
        }
      })
    }
  });

  $('#btnMasterListEntityLookup').click(function () {
    var miningLocationId = getMiningLocationId()
    if (miningLocationId) {
      $('#spinnerMasterListEntityLookup').css('display', 'inline-block');
      appendAlert('Lookup of Mining Location triggered!', 'info');
      $.ajax({
        url: '/api/place/lookupByMiningLocationId',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
          id: miningLocationId
        }),
        complete: function (xhr, status) {
          if (xhr.status === 200) {
            appendAlert('Lookup of Mining Location completed!', 'success');
          } else {
            appendAlert('Sorry, something didn\'t work right.', 'danger');
          }
          $('#spinnerMasterListEntityLookup').css('display', 'none');
        }
      })
    }
  });

  $('#btnLookupAllPlaces').click(function () {
    $('#spinnerBtnLookupAllPlaces').css('display', 'inline-block');
    appendAlert('Lookup of ALL Mining Locations triggered!', 'info');
    $.ajax({
      url: '/api/place/lookupAllPlaces',
      type: 'POST',
      complete: function (xhr, status) {
        if (xhr.status === 200) {
          appendAlert('Lookup of ALL Mining Locations completed!', 'success');
        } else {
          appendAlert('Sorry, something didn\'t work right.', 'danger');
        }
        $('#spinnerBtnLookupAllPlaces').css('display', 'none');
      }
    })
  });

});