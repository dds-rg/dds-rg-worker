$(document).ready(function () {

  var tableInternalPlaces = $('#tableInternalPlaces').DataTable({
    responsive: true,
    pageLength: 50,
    columns: [
      {
        name: 'show',
        orderable: false
      },
      {name: 'name'},
      {name: 'location1'}
    ],
    // paging: false,
    // columns: [
    //   {
    //     orderable:      false,
    //     data:           null,
    //     defaultContent: ''
    //   },
    //   { data: 'name' },
    //   { data: 'location1' }
    // ],
    order: [1, 'asc']
  });

  $('#tableInternalPlaces_length select, #tableInternalPlaces_filter input').addClass('form-control')
  $('#tableInternalPlaces_length select, #tableInternalPlaces_filter input').css('display', 'inline-block')

  //Add event listener for opening and closing details
  // $('#tableInternalPlaces tbody').on('click', function () {
  $('#tableInternalPlaces tbody').on('click', 'td.details-control', function () {
    var tr = $(this).parent()
    var ratingGoogle = tr.attr('data-rg')
    var ratingYelp = tr.attr('data-ry')
    var reviewsGoogle = tr.attr('data-rsg')
    var reviewsYelp = tr.attr('data-rsy')
    var obj = {
      ratingGoogle,
      reviewsGoogle,
      ratingYelp,
      reviewsYelp
    }
    console.log(obj)

    var trD = $(this).parents('tr');
    var row = tableInternalPlaces.row(trD);
    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      row.child('<table><tr><th>Provider</th><th>Rating</th><th>Reviews</th></tr><tr><td>Google</td><td>' + obj.ratingGoogle + '</td><td>' + obj.reviewsGoogle + '</td></tr><tr><td>Yelp</td><td>' + obj.ratingYelp + '</td><td>' + obj.reviewsYelp + '</td></tr></table>').show();
      tr.addClass('shown');
      var jRow = tr.next('tr');
      jRow.css('background-color', 'white')
    }
  });

});