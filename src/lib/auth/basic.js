'use strict'

const url  = require('url')

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const auth = require('basic-auth')
const bcrypt = require('bcrypt')

const _implementation = {}

_implementation.hashComparePromise = function (pass, hash) {
  return new Promise(function (resolve, reject) {
    bcrypt.compare(pass, hash, function (err, res) {
      if (err) return reject(err)
      return resolve(res)
    })
  })
}

const implementation = {}

implementation.addHook = function (_fastify) {

  _fastify.addHook('preHandler', async (req, reply) => {

    function reject(_message) {
      return reply.header('WWW-Authenticate', 'Basic').code(401).send(new Error(_message))
    }

    try {
      let uiuser
      const credentials = auth(req)
      if (!credentials || !credentials.name || !credentials.pass) {
        return reject('No credentials, or malformed credentials, passed.')
      } else {
        log.debug(`received request for ${req.raw.url} from ${credentials.name}`)
        uiuser = await db.models.UIUsers.findOne({
          where: {
            name: credentials.name
          }
        })
        if (!uiuser) {
          return reject(`Credentials could not be validated.`)
        }
        const isAuthentic = await _implementation.hashComparePromise(credentials.pass, uiuser.pass)
        if (!isAuthentic) {
          return reject(`Cretentials for "${credentials.name}" were not determined to be authentic.`)
        }
      }
    } catch (err) {
      log.error(err.message)
      log.debug(err.stack)
      return reject()
    }

  })
}

module.exports = implementation