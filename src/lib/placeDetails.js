'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()


const _implementation = {}

_implementation.persistDetails = async function (_place, _placeDetails) {
  log.trace(`invoking persistDetails`)
  try {
    db.models.SocialProviderPlaceDetails.create({
      masterListEntityId: _place.masterListEntityId,
      socialProviderId: _place.socialProviderId,
      placeId: _place.placeId,
      raw: _placeDetails
    })
  } catch (err) {
    log.error(`problem with persistDetails: ${err.message}`)
    log.debug(err.stack)
  }
}

const implementation = {}

implementation.lookupDetails = async function (_placesArray) {
  log.trace(`invoking lookupDetails`)
  try {
    for (let place of _placesArray) {
      const placeDetails = await require(`~/lib/socialProviders/${place.socialProviderSlug}`).getPlaceDetails(place.placeId)
      _implementation.persistDetails(place, placeDetails)
        .catch(log.warn)
    }
  } catch (err) {
    log.error(`problem with lookupDetails: ${err.message}`)
    log.debug(err.stack)
  }
}

module.exports = implementation