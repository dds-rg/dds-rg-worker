'use strict'

const _util = require('util')

const log = require('~/lib/logger')()

const request = require('request')

const implementation = {}

implementation.makeRequest = function (_obj) {
  return new Promise(function (resolve, reject) {
    log.trace(`makeRequest invoked`)
    request(
      _obj,
      function (error, response, body) {
        const statuscode = response.statusCode.toString()
        log.trace(`makeRequest returned: ${statuscode}`)
        if (error) return reject(error)
        if (statuscode.substring(0, 1) !== '2') {
          return reject(new Error(_util.format('Non 2xx status code: %s, body: %o', statuscode, body)))
        }
        try {
          body = JSON.parse(body)
        } catch (err) {
        }
        return resolve({response, body})
      }
    )
  })
}

module.exports = implementation