'use strict'

const db = require('~/lib/db')

const path = require('path')

const log = require('~/lib/logger.js')()
const config = require('~/config')
require('~/lib/socialProviderImplementations')
const jobs = require('~/lib/jobs')


// hijacks require to allow it to require marko templates
require('marko/node-require')
// tells marko to not output .js "temp" files
require('marko/compiler').defaultOptions.writeToDisk = false

const lasso = require('lasso')
const dir = require('node-dir')
const CronJob = require('cron').CronJob
const fastify = require('fastify')()

const app = {}

app.start = async function () {

try {

  await db.syncModels(false)

  await jobs.refreshJobs()

// bundle up all CSS, LESS, and JS assets
  lasso.configure({
    outputDir: path.join(__dirname, '..', 'public', 'static'),
    urlPrefix: '/public/static',
    plugins: ['lasso-marko', 'lasso-less'],
    // minify: true,
    fingerprintsEnabled: false
  })
  log.trace('finished lasso initialization')

// register templating engine; marko
  await fastify.register(require('point-of-view'), {
    engine: {
      marko: require('marko')
    },
    includeViewExtension: true
  })
  log.trace('finished marko initialization')

// serve all the static files; all of /public
  await fastify.register(require('fastify-static'), {
    root: path.join(__dirname, '..', 'public'),
    prefix: '/public/',
  })
  log.trace('finished static files initialization')

// loop through all files in /plugins and fastify.register(require()) them
  const pluginPaths = await dir.promiseFiles(path.join(__dirname, '..', 'plugins'))
  for (let i = 0; i < pluginPaths.length; i++) {
    log.trace('registering plugin at %s', pluginPaths[i])
    await fastify.register(require(pluginPaths[i]))
  }
  log.trace('finished registering plugins')

  await fastify.listen(3000, '0.0.0.0')
  log.trace('fastify is listening')

  if (process.env['ENVIRONMENT'] && process.env['ENVIRONMENT'] === 'DEV') {
    log.info('ENVIRONMENT is DEV, requiring scratchpad')
    require('~/scratchpad')
  }

  return fastify
} catch (err) {
  log.error(`problem with app.start(): ${err.message}`)
  log.debug(err.stack)
  process.exit(1)
}

}

module.exports = app