'use strict'

const config = require('~/config')
const log = require('~/lib/logger')()

const Sequelize = require('sequelize')

const sequelize = new Sequelize(config.db.connectionString, {
  logging: false
})

const implementation = {}

implementation.sequelize = sequelize

implementation.models = {}

implementation.models.SocialProvider = sequelize.define('social_provider', {
  slug: {
    type: Sequelize.STRING
  },
  name: {
    type: Sequelize.STRING
  },
  clientId: {
    type: Sequelize.STRING
  },
  clientSecret: {
    type: Sequelize.STRING
  },
  active: {
    type:
    Sequelize.BOOLEAN
  }
})

implementation.models.MiningLocation = sequelize.define('mining_location', {
  slug: {
    type: Sequelize.STRING
  },
  name: {
    type: Sequelize.STRING
  },
  latitude: {
    type: Sequelize.STRING
  },
  longitude: {
    type: Sequelize.STRING
  }
})

implementation.models.MasterList = sequelize.define('master_list', {
  name: {
    type: Sequelize.STRING
  },
  isDerived: {
    type: Sequelize.BOOLEAN
  }
})

implementation.models.MasterListEntity = sequelize.define('master_list_entity', {
  name: {
    type: Sequelize.STRING
  },
  location1: {
    type: Sequelize.STRING
  },
  location2: {
    type: Sequelize.STRING
  },
  location3: {
    type: Sequelize.STRING
  }
})

implementation.models.Place = sequelize.define('place', {
  similarityCoefficient: {
    type: Sequelize.FLOAT
  },
  providerId: {
    type: Sequelize.STRING
  }
})

implementation.models.PlaceDetail = sequelize.define('place_detail', {
  providerId: {
    type: Sequelize.STRING
  },
  raw: {
    type: Sequelize.JSON
  }
})

implementation.models.SocialProviderPlaceDetails = sequelize.define('social_provider_place_details', {
  placeId: {
    type: Sequelize.STRING
  },
  raw: {
    type: Sequelize.JSON
  }
})

implementation.models.UIUsers = sequelize.define('ui_users', {
  name: {
    type: Sequelize.STRING
  },
  pass: {
    type: Sequelize.STRING
  }
})

implementation.models.InternalPlace = sequelize.define('internal_place', {
  masterListEntityId: {
    type: Sequelize.INTEGER
  },
  name: {
    type: Sequelize.STRING
  },
  location1: {
    type: Sequelize.STRING
  },
  location2: {
    type: Sequelize.STRING
  },
  location3: {
    type: Sequelize.STRING
  }
})

implementation.models.InternalPlaceAssociation = sequelize.define('intenal_place_association', {
  placeId: {
    type: Sequelize.STRING
  }
})

implementation.models.Job = sequelize.define('job', {
  name: {
    type: Sequelize.STRING
  },
  slug: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  cron: {
    type: Sequelize.STRING
  },
  active: {
    type: Sequelize.BOOLEAN
  }
})

implementation.models.ResponseEntity = sequelize.define('response_entity', {
  providerId: {
    type: Sequelize.STRING
  },
  raw: {
    type: Sequelize.JSONB
  }
})

implementation.models.MasterListEntityAssociation = sequelize.define(
  'master_list_entity_association',
  {
    providerId: {
      type: Sequelize.STRING
    },
    version: {
      type: Sequelize.INTEGER
    }
  },
  {
    indexes: [
      {
        name: 'master_list_entity_associations_ik',
        unique: true,
        fields: [
          'providerId',
          'masterListEntityId',
          'socialProviderId'
        ]
      }
    ]
  }
)

implementation.models.MasterListEntityLookupAttempt = sequelize.define('master_list_entity_lookup_attempt', {
  attempts: {
    type: Sequelize.INTEGER,
    defaultValue: 1
  }
})


implementation.syncModels = async function (force = false) {

  try {
    implementation.models.MasterList.belongsTo(implementation.models.MiningLocation)
    implementation.models.MasterListEntity.belongsTo(implementation.models.MasterList)

    implementation.models.SocialProviderPlaceDetails.belongsTo(implementation.models.SocialProvider)
    implementation.models.SocialProviderPlaceDetails.belongsTo(implementation.models.MasterListEntity)

    implementation.models.Place.belongsTo(implementation.models.MasterListEntity)
    implementation.models.Place.belongsTo(implementation.models.SocialProvider)

    implementation.models.Place.belongsTo(implementation.models.SocialProvider)
    implementation.models.Place.belongsTo(implementation.models.MasterListEntity)

    implementation.models.PlaceDetail.belongsTo(implementation.models.SocialProvider)
    implementation.models.PlaceDetail.belongsTo(implementation.models.MasterListEntity)

    implementation.models.InternalPlaceAssociation.belongsTo(implementation.models.InternalPlace)
    implementation.models.InternalPlaceAssociation.belongsTo(implementation.models.SocialProvider)

    implementation.models.ResponseEntity.belongsTo(implementation.models.SocialProvider)
    implementation.models.ResponseEntity.belongsTo(implementation.models.MiningLocation)

    implementation.models.MasterListEntityAssociation.belongsTo(implementation.models.MasterListEntity)
    implementation.models.MasterListEntityAssociation.belongsTo(implementation.models.SocialProvider)

    implementation.models.MasterListEntityLookupAttempt.belongsTo(implementation.models.MasterListEntity)
    implementation.models.MasterListEntityLookupAttempt.belongsTo(implementation.models.SocialProvider)

    await implementation.models.MiningLocation.sync()
    await implementation.models.MasterList.sync()
    await implementation.models.MasterListEntity.sync()
    await implementation.models.SocialProvider.sync()
    await implementation.models.UIUsers.sync()
    await implementation.models.Job.sync()

    await implementation.models.Place.sync()
    await implementation.models.PlaceDetail.sync()

    await implementation.models.SocialProviderPlaceDetails.sync()

    await implementation.models.InternalPlace.sync()
    await implementation.models.InternalPlaceAssociation.sync()

    await implementation.models.ResponseEntity.sync()

    await implementation.models.MasterListEntityAssociation.sync()
    await implementation.models.MasterListEntityLookupAttempt.sync()

  } catch (err) {
    log.error(`problem syncing models: ${err.message}`)
    log.debug(err.stack)
    process.exit(1)
  }

}

module.exports = implementation