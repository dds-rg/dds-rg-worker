'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const implementation = {}

implementation.getMasterListsByMiningLocation = async function (_miningLocation) {

  return await db.models.MasterList.findAll({
    where: {
      miningLocationId: _miningLocation.id
    }
  })

}

module.exports = implementation
