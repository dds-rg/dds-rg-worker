'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const implementation = {}

implementation.create = async function (_providerId, _masterListEntityId, _socialProviderId) {

  return await db.sequelize.query(
      `
    select *
      from f_insert_master_list_entity_association(p_provider_id := :providerId,
                                                   p_master_list_entity_id := :masterListEntityId,
                                                   p_social_provider_id := :socialProviderId)
    `,
    {
      replacements: {
        providerId: _providerId,
        masterListEntityId: _masterListEntityId,
        socialProviderId: _socialProviderId
      }
    }
  )

}

module.exports = implementation
