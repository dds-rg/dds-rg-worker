'use strict'

const stringSimilarity = require('string-similarity')
const roundTo = require('round-to')

const db = require('~/lib/db')
const log = require('~/lib/logger')()
const MasterList = require('~/lib/models/masterList')
const MasterListEntity = require('~/lib/models/masterListEntity')

const implementation = {}

// async function chooseProviderPlace(_keyword, _providerPlaces) {
//
//   let providerPlacesComparisonArray = []
//
//   for (let providerPlace of _providerPlaces) {
//     const keywordComparisonString = providerPlace.keywordComparisonString.replace(/[^A-Za-z0-9_]/g, "")
//     let similarityCoefficient = stringSimilarity.compareTwoStrings(_keyword, keywordComparisonString)
//     log.trace(`    "${_keyword}" and "${keywordComparisonString}" (providerId ${providerPlace.providerId}) share a similarity of ${roundTo((similarityCoefficient * 100), 2)}%`)
//     providerPlacesComparisonArray.push({
//       providerId: providerPlace.providerId,
//       similarityCoefficient
//     })
//
//   }
//
//   providerPlacesComparisonArray = providerPlacesComparisonArray.sort(function (a, b) {
//     return b.similarityCoefficient - a.similarityCoefficient
//   })
//
//   return providerPlacesComparisonArray[0]
// }

// async function createPlace(_masterListEntity, _socialProvider, _providerPlace, _raw) {
//
//   await db.models.Place.create({
//     masterListEntityId: _masterListEntity.id,
//     socialProviderId: _socialProvider.id,
//     providerId: _providerPlace.providerId,
//     similarityCoefficient: _providerPlace.similarityCoefficient,
//     raw: _raw
//   })
//
// }

// implementation.lookupPlacesByMiningLocationSocialProviderAndMasterListEntity = async function (_masterListEntity, _socialProvider, _miningLocation) {
//
//   const masterListEntityComparisonString = (`${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`).replace(/[^A-Za-z0-9_]/g, "")
//
//   const socialProviderImplementation = require(`~/lib/socialProviderImplementations/${_socialProvider.slug}`)
//
//   const {raw, transformedRaw} = await socialProviderImplementation.lookupPlaces(_masterListEntity, _miningLocation)
//
//   const providerPlace = await chooseProviderPlace(masterListEntityComparisonString, transformedRaw)
//
//   if (providerPlace) {
//     log.debug(`place ${}`)
//     await createPlace(_masterListEntity, _socialProvider, providerPlace, raw)
//   }
//
// }

implementation.lookupAllPlaceDetails = async function () {

  //TODO
  try {

    const socialProviders = await db.models.SocialProvider.findAll()

    for (let miningLocation of miningLocations) {
      const masterLists = await MasterList.getMasterListsByMiningLocation(miningLocation)
      for (let masterList of masterLists) {
        const masterListEntities = await MasterListEntity.getMasterListEntitiesByMasterList(masterList)
        for (let masterListEntity of masterListEntities) {
          for (let socialProvider of socialProviders) {
            log.debug(`starting place lookup on "${masterListEntity.name}" with "${socialProvider.slug}"`)
            const places = await implementation.lookupPlacesByMiningLocationSocialProviderAndMasterListEntity(
              masterListEntity,
              socialProvider,
              miningLocation
            )
          }
        }
      }
    }
  } catch (err) {
    log.error(`problem running "lookupAllPlaceDetails": ${err.message}`)
    log.debug(err.stack)
  }

}

module.exports = implementation