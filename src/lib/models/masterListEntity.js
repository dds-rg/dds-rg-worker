'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const implementation = {}

implementation.getMasterListEntitiesByMasterList = async function (_masterList) {

  return await db.models.MasterListEntity.findAll({
    where: {
      masterListId: _masterList.id
    }

  })

}

module.exports = implementation