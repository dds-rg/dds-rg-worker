'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const implementation = {}

implementation.create = async function (_socialProviderId, _providerId, _raw, _miningLocationId) {

  if (_raw.businesses) {
    for (let responseEntity of _raw.businesses) {
      await db.sequelize.query(
          `
            SELECT *
            FROM f_insert_response_entity(:socialProviderId, :providerId, :raw::JSONB, :miningLocationId)
        `,
        {
          replacements: {
            socialProviderId: _socialProviderId,
            providerId: responseEntity.id,
            raw: JSON.stringify(responseEntity),
            miningLocationId: _miningLocationId
          }
        }
      )
    }
  }

}

module.exports = implementation
