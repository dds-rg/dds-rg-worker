'use strict'

const stringSimilarity = require('string-similarity')
const roundTo = require('round-to')

const db = require('~/lib/db')
const log = require('~/lib/logger')()
const MasterList = require('~/lib/models/masterList')
const MasterListEntity = require('~/lib/models/masterListEntity')
const MasterListEntityAssociation = require('~/lib/models/masterListEntityAssociation')
const MasterListEntityAttempt = require('~/lib/models/masterListEntityAttempt')
const ResponseEntity = require('~/lib/models/responseEntity')

const implementation = {}

async function chooseProviderPlace (_keyword, _providerPlaces) {
  _keyword = _keyword.toLowerCase()

  let providerPlacesComparisonArray = []

  for (let providerPlace of _providerPlaces) {
    const keywordComparisonString = providerPlace.keywordComparisonString.replace(/[^A-Za-z0-9_]/g, "").toLowerCase()
    let similarityCoefficient = stringSimilarity.compareTwoStrings(_keyword, keywordComparisonString)
    log.trace(`     "${_keyword}" and "${keywordComparisonString}" (providerId ${providerPlace.providerId}) share a similarity of ${roundTo((similarityCoefficient * 100), 2)}%`)
    providerPlacesComparisonArray.push({
      providerId: providerPlace.providerId,
      similarityCoefficient
    })

  }

  providerPlacesComparisonArray = providerPlacesComparisonArray.sort(function (a, b) {
    return b.similarityCoefficient - a.similarityCoefficient
  })

  return providerPlacesComparisonArray[0]
}

async function createPlace (_masterListEntity, _socialProvider, _providerPlace) {

  return await db.models.Place.create({
    masterListEntityId: _masterListEntity.id,
    socialProviderId: _socialProvider.id,
    providerId: _providerPlace.providerId,
    similarityCoefficient: _providerPlace.similarityCoefficient
  })

}

// implementation.lookupPlacesByMiningLocationSocialProviderAndMasterListEntity = async function (_masterListEntity, _socialProvider, _miningLocation) {
//
//   const masterListEntityComparisonString = (`${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`).replace(/[^A-Za-z0-9_]/g, "")
//
//   const socialProviderImplementation = require(`~/lib/socialProviderImplementations/${_socialProvider.slug}`)
//
//   const {raw, transformedRaw} = await socialProviderImplementation.lookupPlaces(_masterListEntity, _miningLocation)
//
//   const providerPlace = await chooseProviderPlace(masterListEntityComparisonString, transformedRaw)
//
//   if (providerPlace) {
//     await createPlace(_masterListEntity, _socialProvider, providerPlace, raw)
//   }
//
// }

implementation.lookupMasterListEntityBySocialProvider = async function (_masterListEntity, _socialProvider) {

  try {
    const masterList = await db.models.MasterList.findOne({
      where: {
        id: _masterListEntity.masterListId
      }
    })
    log.debug('masterList: %o', masterList)

    const miningLocation = await db.models.MiningLocation.findOne({
      where: {
        id: masterList.miningLocationId
      }
    })
    log.debug('miningLocation: %o', miningLocation)

    MasterListEntityAttempt.create(_masterListEntity.id, _socialProvider.id)
      .then(function (_masterListEntityAttempt) {
        log.trace('masterListEntityAttempt created: %o', _masterListEntityAttempt)
      })
      .catch(function (err) {
        log.error(`masterListEntityAttempt was not created: ${err.message}`)
      })

    const masterListEntityComparisonString = (`${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`).replace(/[^A-Za-z0-9_]/g, "")

    const socialProviderImplementation = require(`~/lib/socialProviderImplementations/${_socialProvider.slug}`)

    const {raw, transformedRaw} = await socialProviderImplementation.lookupPlaces(_masterListEntity, miningLocation)

    log.trace(`raw: %o`, raw)

    log.debug('finished socialProviderImplementation.lookupPlaces')

    const providerPlace = await chooseProviderPlace(masterListEntityComparisonString, transformedRaw)

    if (providerPlace) {
      if (providerPlace.similarityCoefficient >= 0.50) {
        log.debug(`   providerId ${providerPlace.providerId} has been chosen, with a similarity coefficient of ${roundTo((providerPlace.similarityCoefficient * 100), 2)}%\n`)
        await createPlace(_masterListEntity, _socialProvider, providerPlace, raw)
        // intentionally not awaiting
        log.debug('_socialProvider.id, providerPlace.providerId, miningLocation.id (%s, %s, %s)', _socialProvider.id, providerPlace.providerId, miningLocation.id)
        ResponseEntity.create(_socialProvider.id, providerPlace.providerId, raw, miningLocation.id)
          .then(function () {
            log.trace('responseEntity objects created')
          })
          .catch(function (err) {
            log.error(`responseEntity was not created: ${err.message}`)
          })
        MasterListEntityAssociation.create(providerPlace.providerId, _masterListEntity.id, _socialProvider.id)
          .then(function (_masterListEntityAssociation) {
            log.trace('masterListEntityAssociation created: %o', _masterListEntityAssociation)
          })
          .catch(function (err) {
            log.error(`masterListEntityAssociation was not created: ${err.message}`)
          })
      } else {
        log.debug(`   providerId ${providerPlace.providerId} has not been persisted because similarity coefficient is too low (${roundTo((providerPlace.similarityCoefficient * 100), 2)}%)\n`)
      }
      // await db.sequelize.query(
      //     `
      //       INSERT INTO DERIVED_PLACE(PROVIDERID, SOCIALPROVIDERID, MININGLOCATIONID, LASTUPDATEDATE, LASTMINEDATE)
      //       VALUES (:providerId, :socialProviderId, :miningLocationId, now(), now())
      //       ON CONFLICT ON CONSTRAINT DERIVED_PLACE_PK DO UPDATE SET LASTUPDATEDATE = now()
      //   `,
      //   {
      //     replacements: {
      //       providerId: providerPlace.providerId,
      //       socialProviderId: _socialProvider.id,
      //       miningLocationId: miningLocation.id
      //     }
      //   }
      // )
    }
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }

}

implementation.createDerivedPlaces = async function (_derivedMasterList) {

  try {
    log.debug(`inserting derived places into master list "${_derivedMasterList.name}"`)

    //const refreshSuccessResult = await db.sequelize.query(`SELECT f_refresh_basic_tables() as "refreshSuccess";`)

    //log.trace(`refreshSuccess: ${refreshSuccessResult[0][0].refreshSuccess}`)

    const resultDerivedList = await db.sequelize.query(
      'select f_populate_derived_master_lists() as "resultDerivedList"'
    )

    log.debug(`master list "${_derivedMasterList.name}" insert success: ${resultDerivedList[0][0].resultDerivedList}`)

    return resultDerivedList
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }

}

implementation.lookupByMasterList = async function (_masterList, _inTheLast = '1 day') {

  try {
    log.debug(` finding Master List Entities for "${_masterList.name}" that have not been mined in the last "${_inTheLast}"`)

    const miningLocation = await db.models.MiningLocation.findOne({
      where: {
        id: _masterList.miningLocationId
      }
    })

    const socialProviders = await db.models.SocialProvider.findAll({
      where: {
        active: true
      }
    })

    const masterListEntities = (await db.sequelize.query(
        `
          SELECT *
          FROM f_master_list_entities_to_be_looked_up(P_MASTER_LIST_ID := :masterListId,
                                                      P_IN_THE_LAST := INTERVAL :inTheLast)
      `,
      {
        replacements: {
          masterListId: _masterList.id,
          inTheLast: _inTheLast
        }
      }
    ))[0]

    log.debug(`  ${masterListEntities.length} Master List Entities being processed for "${_masterList.name}"`)
    for (let masterListEntity of masterListEntities) {
      for (let socialProvider of socialProviders) {
        log.debug(`   starting place lookup on "${masterListEntity.name}" with "${socialProvider.slug}"`)
        const places = await implementation.lookupMasterListEntityBySocialProvider(
          masterListEntity,
          socialProvider
        )
      }
    }
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }

}

implementation.updateLocationPlaceTables = async function () {
  log.trace(`in updateLocationPlaceTables`)
  const updateLocationPlaceTables = await db.sequelize.query(`
    DO $$
    BEGIN
      PERFORM f_update_internal_place_tables();
    END;
    $$ LANGUAGE plpgsql;
  `)
  log.trace('updateLocationPlaceTables: %o', updateLocationPlaceTables)
}

implementation.lookupByMiningLocation = async function (_miningLocation) {

  try {
    log.debug(`beginning mining for "${_miningLocation.name}"`)

    const masterLists = await db.models.MasterList.findAll({
      where: {
        miningLocationId: _miningLocation.id
      },
      order: [
        ['isDerived', 'DESC']
      ]
    })

    log.debug('masterLists: %o', masterLists)

    for (let masterList of masterLists) {
      await implementation.lookupByMasterList(masterList)
    }

    const derivedMasterList = await db.models.MasterList.findOne({
      where: {
        name: 'derivedPlaces' + _miningLocation.slug.charAt(0).toUpperCase() + _miningLocation.slug.slice(1)
      }
    })
    await implementation.createDerivedPlaces(derivedMasterList)
    //await implementation.updateLocationPlaceTables()

    log.debug(`ended mining for "${_miningLocation.name}"`)
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }

}

implementation.lookupAllPlaces = async function () {
  const beginTime = new Date()
  try {
    const miningLocations = await db.models.MiningLocation.findAll()

    for (let miningLocation of miningLocations) {
      await implementation.lookupByMiningLocation(miningLocation)
    }
  } catch (err) {
    log.error(`problem running "lookupAllPlaces": ${err.message}`)
    log.debug(err.stack)
  } finally {
    const elapsedSeconds = ((new Date()) - beginTime) / 1000
    log.debug(`*** finished "lookupAllPlaces" in ${elapsedSeconds} seconds (${roundTo(elapsedSeconds / 60, 2)} minutes)`)
  }

}

implementation.lookupAllDerivedPlaces = async function () {
  try {
    const miningLocations = await db.models.MiningLocation.findAll()

    for (let miningLocation of miningLocations) {
      await implementation.lookupByMiningLocation(miningLocation)
    }

  } catch (err) {
    log.error(`problem running "lookupAllPlaces": ${err.message}`)
    log.debug(err.stack)
  }

}

module.exports = implementation