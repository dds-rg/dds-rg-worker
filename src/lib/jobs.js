'use strict'

const db = require('~/lib/db')
const placeDetails = require('~/lib/placeDetails')
// const socialProviders = require('~/lib/socialProviders')
const log = require('~/lib/logger.js')()
const Place = require('~/lib/models/place')

const CronJob = require('cron').CronJob
const cronstrue = require('cronstrue')

const implementation = {}

implementation.activeJobs = {}

implementation.refreshJobs = async function () {
  try {
    for (let activeJob of Object.keys(implementation.activeJobs)) {
      implementation.activeJobs[activeJob].stop()
      delete implementation.activeJobs[activeJob]
      log.trace(`${activeJob} stopped and deleted`)
    }
    const jobs = (await db.models.Job.findAll({
      where: {
        active: true
      }
    }))
    for (let job of jobs) {
      if (!implementation.activeJobs[job.slug]) {
        implementation.activeJobs[job.slug] = new CronJob({
          cronTime: job.cron,
          onTick: implementation.jobActions[job.slug],
          onComplete: function () {
            try {
              log.trace(`job "${job.slug}" has run (or been stopped)`)
            } catch (e) {
              log.warn(e.message)
              log.debug(e.stack)
            }
          }
        })
        implementation.activeJobs[job.slug].start()
        log.trace(`scheduled job "${job.name}" set to run "${cronstrue.toString(job.cron)}", next run time: ${implementation.activeJobs[job.slug].nextDates()}`)
      }
    }
  } catch (e) {
    log.error(e.message)
    log.debug(e.stack)
  }
}

const refresherJob = new CronJob({
  cronTime: '* * * * *',
  onTick: async function () {
    log.trace(`running refresherJob`)
    await implementation.refreshJobs()
  }
})
refresherJob.start()

implementation.jobActions = {

  hourlyPlaceReviewScrape: async function (onComplete) {
    try {
      const places = (await db.sequelize.query(`
        SELECT *
        FROM VW_MASTER_LIST_ENTITY_LOOKUPS
      `))[0]

      await placeDetails.lookupDetails(places)
    } catch (err) {
      log.error(`problem running job "daily place detail scrape": ${err.message}`)
      log.debug(err.stack)
    } finally {
      onComplete()
    }
  },

  hourlyLookupAllMiningLocations: async function (onComplete) {
    try {
      const miningLocations = await db.models.MiningLocation.findAll()

      for (let miningLocation of miningLocations) {
        await Place.lookupByMiningLocation(miningLocation)
        await db.sequelize.query('select f_populate_internal_place_tables();')
      }

    } catch (err) {
      log.error(`problem running job "hourlyLookupAllMiningLocations": ${err.message}`)
      log.debug(err.stack)
    } finally {
      onComplete()
    }
  },

  lookupAllPlaces: Place.lookupAllPlaces,

  secondTest: async function () {
    log.trace(`secondTest job called`)
  }

}

module.exports = implementation