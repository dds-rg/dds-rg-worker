'use strict'

const db = require('~/lib/db')
const util = require('~/lib/util')
const log = require('~/lib/logger')()

const _implementation = {}
const implementation = {
  initialized: false
}

// identify the provider
_implementation.slug = 'yelp'

_implementation.getSocialProvider = async function () {
  return await db.models.SocialProvider.findOne({
    where: {
      slug: _implementation.slug
    }
  })
}

// // maybe you want to persist the client or something
// _implementation.getClient = async function () {
//
// }

// should return raw results from looking up the entity with the social provider API
// {searchResults: []}
_implementation.lookupPlaces = async function (_keyword, _miningLocation) {

  const {body} = await util.makeRequest({
    uri: 'https://api.yelp.com/v3/businesses/search',
    method: 'GET',
    qs: {
      latitude: _miningLocation.latitude,
      longitude: _miningLocation.longitude,
      term: _keyword,
      radius: '40000'
    },
    headers: {
      authorization: `Bearer ${implementation.socialProvider.clientSecret}`
    }
  })

  return body

}

// should return raw place details from social provider API
// {someId: '342refw4r23ef', reviews:[]}
_implementation.lookupPlaceDetails = async function (_providerId) {

  const {body} = await util.makeRequest({
    uri: `https://api.yelp.com/v3/businesses/${_providerId}/reviews`,
    method: 'GET',
    headers: {
      authorization: `Bearer ${implementation.socialProvider.clientSecret}`
    }
  })
  return body

}

implementation.init = async function () {
  // whatever initialization needs to be done, like adding the client to the _implementation, for ease of use later
  // if that helps in some way
  implementation.socialProvider = await _implementation.getSocialProvider()
  implementation.initialized = true
}

// must return an array of {keywordComparisonString, providerId} objects:
// [{
//   keywordComparisonString: 'Some PlaceSome Address1Some Address2',
//   providerId: 'dsfwefnkj2390fsdkj'
// }]
implementation.transformRaw = async function (_raw) {

  return _raw.businesses.map(function (result) {
    return {
      keywordComparisonString: (`${result.name} ${result.location.address1 || ''} ${result.location.address2 || ''} ${result.location.address3 || ''}`).replace(/[^A-Za-z0-9_]/g, ""),
      providerId: result.id
    }
  })

}

implementation.lookupPlaces = async function (_masterListEntity, _miningLocation) {

  const keyword = `${_masterListEntity.name}`

  const raw = await _implementation.lookupPlaces(keyword, _miningLocation)

  const transformedRaw = await implementation.transformRaw(raw)

  return {
    raw,
    transformedRaw
  }

}

implementation.lookupPlaceDetails = _implementation.lookupPlaceDetails

module.exports = implementation