'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const implementation = {}

implementation.socialProviderImplementations = [
  {
    slug: 'google',
    implementation: require('~/lib/socialProviderImplementations/google')
  },
  {
    slug: 'yelp',
    implementation: require('~/lib/socialProviderImplementations/yelp')
  },
  {
    slug: 'facebook',
    implementation: require('~/lib/socialProviderImplementations/facebook')
  }
]

!async function () {

  try {
    for (let socialProviderImplementation of implementation.socialProviderImplementations) {
      log.trace(`initializing social provider implementation "${socialProviderImplementation.slug}"`)
      await socialProviderImplementation.implementation.init()
    }
  } catch (err) {
    log.error(`problem initializing social providers: ${err.message}`)
    log.debug(err)
  }

}()

// const _implementation = {}
//
// _implementation.getMiningLocation = async function (_miningLocationSlug) {
//
//   return await db.models.MiningLocation.findOne({
//     where: {
//       slug: _miningLocationSlug
//     }
//   })
//
// }
//
// _implementation.getMasterList = async function (_miningLocation) {
//
//   return await db.models.MasterList.findAll({
//     where: {
//       miningLocationId: _miningLocation.id
//     }
//   })
//
// }
//
// _implementation.getMasterListEntities = async function (_masterList) {
//
//   return await db.models.MasterListEntity.findAll({
//     where: {
//       masterListId: _masterList.id
//     }
//
//   })
//
// }
//
// implementation.lookupByMiningLocation = async function (_miningLocation) {
//
//   try {
//
//     log.info(`beginning lookup for mining location "${_miningLocation.name}"`)
//
//     // get all master lists for a location
//     const masterLists = await _implementation.getMasterList(_miningLocation)
//
//     // loop throught the master lists
//     for (let masterList of masterLists) {
//
//       // get all of the entites belonging to the master list
//       const masterListEntities = await _implementation.getMasterListEntities(masterList)
//       log.debug(`looping through masterListEntries in masterList "${masterList.name}"`)
//
//       // loop through each entity
//       for (let masterListEntity of masterListEntities) {
//         log.debug(`processing "${masterListEntity.name}", id: ${masterListEntity.id}`)
//         // call the social provider's lookup method to actually preform the lookup
//         for (let socialProvider of implementation.socialProviders) {
//           log.debug(`attempting "${socialProvider.slug}" lookup of "${masterListEntity.name}", id: ${masterListEntity.id}`)
//           await socialProvider.implementation.lookup(masterListEntity, _miningLocation)
//         }
//       }
//
//     }
//   } catch (err) {
//     log.error(`problem with lookup for mining location "${_miningLocation.slug}": ${err.message}`)
//     log.debug(err.stack)
//   }
//
// }

module.exports = implementation