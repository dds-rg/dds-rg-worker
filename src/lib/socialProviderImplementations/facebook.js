'use strict'

const db = require('~/lib/db')
const util = require('~/lib/util')
const log = require('~/lib/logger')()

const _implementation = {}
const implementation = {}

// identify the provider
_implementation.slug = 'facebook'

_implementation.getSocialProvider = async function () {
  return await db.models.SocialProvider.findOne({
    where: {
      slug: _implementation.slug
    }
  })
}

// // maybe you want to persist the client or something
// _implementation.getClient = async function () {
//
// }

// should return raw results from looking up the entity with the social provider API
// {searchResults: []}
_implementation.lookupPlaces = async function (_keyword, _miningLocation) {

  try {
    log.trace('getFacebookData running -> %s', _keyword)
    const fbAccessTokenResults = await util.makeRequest({
      uri: 'https://graph.facebook.com/oauth/access_token',
      method: 'GET',
      qs: {
        client_id: _implementation.socialProvider.clientId,
        client_secret: _implementation.socialProvider.clientSecret,
        redirect_uri: 'https://auth.charlesread.io',
        grant_type: 'client_credentials'
      }
    })
    const fbAccessToken = fbAccessTokenResults.body.access_token
    log.trace('fbAccessTokenResults.body: %j', fbAccessTokenResults.body)
    const results = await util.makeRequest({
      uri: 'https://graph.facebook.com/search',
      method: 'GET',
      qs: {
        access_token: fbAccessToken,
        q: _keyword,
        center: `${_miningLocation.latitude}, ${_miningLocation.longitude}`,
        distance: '50000',
        fields: 'name,location,rating_count,overall_star_rating',
        type: 'place'
      }
    })
    return results
  } catch (err) {
    log.warn(err.message)
  }


}

// should return raw place details from social provider API
// {someId: '342refw4r23ef', reviews:[]}
_implementation.lookupPlaceDetails = async function (_placeId) {

}

implementation.init = async function () {
  // whatever initialization needs to be done, like adding the client to the _implementation, for ease of use later
  // if that helps in some way
  _implementation.socialProvider = await _implementation.getSocialProvider()
}

// must return an array of {keywordComparisonString, providerId} objects:
// [{
//   keywordComparisonString: 'Some PlaceSome Address1Some Address2',
//   providerId: 'dsfwefnkj2390fsdkj'
// }]
implementation.transformRaw = async function (_raw) {

  function createAddressString(_location) {
    let addressString = ''
    addressString += _location.street ? `${_location.street} ` : ''
    addressString += _location.city ? `${_location.city} ` : ''
    addressString += _location.state ? `${_location.state} ` : ''
    addressString += _location.zip ? `${_location.zip} ` : ''
    addressString += _location.country ? `${_location.country} ` : ''
    return addressString
  }

  return _raw.body.data.map(function (result) {
    return {
      keywordComparisonString: `${result.name} ${createAddressString(result.location)}`,
      providerId: result.id
    }
  })

}

implementation.lookupPlaces = async function (_masterListEntity, _miningLocation) {

  const keyword = `${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`

  const raw = await _implementation.lookupPlaces(keyword, _miningLocation)

  // console.log(raw)

  const transformedRaw = await implementation.transformRaw(raw)

  // console.log(transformedRaw)

  return {
    raw,
    transformedRaw
  }

}

module.exports = implementation