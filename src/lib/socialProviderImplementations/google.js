'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const _implementation = {}
const implementation = {
  initialized: false
}

// identify the provider
_implementation.slug = 'google'

_implementation.getSocialProvider = async function () {
  return await db.models.SocialProvider.findOne({
    where: {
      slug: _implementation.slug
    }
  })
}

_implementation.getClient = async function () {
  log.trace(`beginning _implementation.getClient`)
  let client
  try {
    client = require('@google/maps').createClient({
      key: implementation.socialProvider.clientSecret,
      Promise: Promise
    })
    log.trace(`ended _implementation.getClient`)
    return client
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }
}

_implementation.lookupPlaces = async function (_keyword, _miningLocation) {
  log.trace(`beginning _implementation.lookupPlaces, _keyword: %s, _miningLocation: %o`, _keyword, _miningLocation)
  let places
  try {
    places = await implementation.client.placesNearby({
      keyword: _keyword,
      location: `${_miningLocation.latitude},${_miningLocation.longitude}`,
      radius: 50000
    }).asPromise()
    console.log(places)
    log.trace('ended _implementation.lookupPlaces successfully')
    return places
  } catch (err) {
    log.error(err.message)
    log.debug(err.stack)
  }
}

_implementation.lookupPlaceDetails = async function (_providerId) {

  return await implementation.client.place({
    placeid: _providerId
  }).asPromise()

}

implementation.init = async function () {
  implementation.socialProvider = await _implementation.getSocialProvider()
  implementation.client = await _implementation.getClient()
  implementation.initialized = true
}

implementation.transformRaw = async function (_raw) {

  return _raw.json.results.map(function (result) {
    return {
      keywordComparisonString: `${result.name}${result.vicinity}`,
      providerId: result.place_id
    }
  })

}

implementation.lookupPlaces = async function (_masterListEntity, _miningLocation) {

  const keyword = `${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`

  const raw = await _implementation.lookupPlaces(keyword, _miningLocation)

  const transformedRaw = await implementation.transformRaw(raw)

  return {
    raw,
    transformedRaw
  }

}

implementation.lookupPlaceDetails = _implementation.lookupPlaceDetails

module.exports = implementation