'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()

const _implementation = {}
const implementation = {}

// identify the provider
_implementation.slug = ''

_implementation.getSocialProvider = async function () {
  return await db.models.SocialProvider.findOne({
    where: {
      slug: _implementation.slug
    }
  })
}

// // maybe you want to persist the client or something
// _implementation.getClient = async function () {
//
// }

// should return raw results from looking up the entity with the social provider API
// {searchResults: []}
_implementation.lookupPlaces = async function (_keyword, _miningLocation) {

  return await implementation.client.placesNearby({
    keyword: _keyword,
    location: `${_miningLocation.latitude},${_miningLocation.longitude}`,
    radius: 50000
  }).asPromise()

}

// should return raw place details from social provider API
// {someId: '342refw4r23ef', reviews:[]}
_implementation.lookupPlaceDetails = async function (_placeId) {

}

implementation.init = async function () {
  // whatever initialization needs to be done, like adding the client to the _implementation, for ease of use later
  // if that helps in some way
}

// must return an array of {keywordComparisonString, providerId} objects:
// [{
//   keywordComparisonString: 'Some PlaceSome Address1Some Address2',
//   providerId: 'dsfwefnkj2390fsdkj'
// }]
implementation.transformRaw = async function (_raw) {

  return _raw.json.results.map(function (result) {
    return {
      keywordComparisonString: `${result.name}${result.vicinity}`,
      providerId: result.place_id
    }
  })

}

implementation.lookupPlaces = async function (_masterListEntity, _miningLocation) {

  const keyword = `${_masterListEntity.name} ${_masterListEntity.location1 || ''} ${_masterListEntity.location2 || ''} ${_masterListEntity.location3 || ''}`

  const raw = await _implementation.lookupPlaces(keyword, _miningLocation)

  const transformedRaw = await implementation.transformRaw(raw)

  return {
    raw,
    transformedRaw
  }

}

module.exports = implementation