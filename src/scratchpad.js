'use strict'

const db = require('~/lib/db')
const log = require('~/lib/logger')()
// const socialProviders = require('~/lib/socialProviders')

const facebook = require('~/lib/socialProviderImplementations/facebook')

const Place = require('~/lib/models/place')

async function getMasterList(_miningLocation) {

  return await db.models.MasterList.findAll({
    where: {
      miningLocationId: _miningLocation.id
    }
  })

}

async function getMasterListEntities(_masterList) {

  return await db.models.MasterListEntity.findAll({
    where: {
      masterListId: _masterList.id
    }

  })

}

!async function () {

  // const miningLocation = await db.models.MiningLocation.findOne({
  //   where: {
  //     slug: 'paris'
  //   }
  // })
  //
  // const res = await facebook.lookupPlaces({
  //   name: 'boulangerie julien'
  // }, miningLocation)

  // try {
  //
  //   await Place.lookupAllPlaces()
  //
  // } catch (err) {
  //   console.error('***********************************')
  //   log.error(err.message)
  //   log.debug(err.stack)
  //   console.error('***********************************')
  // } finally {
  //   log.debug(`--  END --`)
  // }

}()
  .catch(console.error)