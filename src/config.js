'use strict'

const config = {}

config.db = {
  connectionString: process.env['CONNECTION_STRING']
}

config.logger = {
  pretty: true,
  translateTime: true,
  level: 'trace'
}

module.exports = config