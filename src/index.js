'use strict'

require('require-self-ref')

const app = require('~/lib/app.js')
const log = require('~/lib/logger.js')()

function logError(err) {
  log.error(err.message)
  log.debug(err.stack)
}

!async function () {
  try {
    const fastify = await app.start()
    log.info('server listening on port %s', fastify.server.address().port)
    process.on('SIGINT', function () {
      log.info('SIGINT caught, shutting fastify down gracefully')
      fastify.close(process.exit)
    })
  } catch (err) {
    logError(err)
  }
}()
  .catch(function (err) {
    log.warn(`ERROR SHOULD NOT HAVE BUBBLED UP THIS FAR!!!`)
    logError(err)
  })
